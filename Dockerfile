FROM node:11 as builder
WORKDIR /app
COPY package*.json /app/
COPY ./ /app/
RUN npm install
RUN npm run build

FROM nginx:1.17-alpine
ENV REACT_APP_SERVER_URL='https://spring-demo-ds2020-vladmihali.herokuapp.com/'
RUN apk --no-cache add curl
RUN curl -L https://github.com/a8m/envsubst/releases/download/v1.1.0/envsubst-`uname`-`uname -m` -o envsubst && \
    chmod +x envsubst && \
    mv envsubst /usr/local/bin
COPY --from=builder /app/nginx.conf /etc/nginx/nginx.template
CMD ["/bin/sh", "-c", "envsubst < /etc/nginx/nginx.template > etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"]
COPY --from=builder /app/build/ /usr/share/nginx/html