import axios from "axios";
import authHeader from "./auth-header";

const API_URL = 'https://spring-demo-ds2020-vladmihali.herokuapp.com/'+"energy";

const getMeasurementBoard = () => {
  return axios.get(API_URL + "/", { headers: authHeader() });
};

const getMeasurementBoardbyDeviceId = (deviceId) => {
    return axios.get(API_URL + "/device/"+deviceId, { headers: authHeader() });
};

const getMyMeasurementBoardbyDeviceId = (deviceId) => {
    return axios.get(API_URL + "/my/device/"+deviceId, { headers: authHeader() });
};

const getMyMeasurementBoardbyDeviceIdAndDate = (deviceId, date) => {
    const dateDto = {
        date: date
    };
    return axios.post(API_URL + "/my/device/"+deviceId, dateDto, { headers: authHeader() });
};

const updateMeasurement = (newData) => {
    return axios.put(API_URL + "/"+newData.id, newData, { headers: authHeader() });
};

const addMeasurement = (newData) => {
    return axios.post(API_URL + "/", newData, { headers: authHeader() });
};

const deleteMeasurement = (id) => {
    return axios.delete(API_URL + "/"+id, { headers: authHeader() });
};
export default {
    getMeasurementBoard,
    updateMeasurement,
    addMeasurement,
    deleteMeasurement,
    getMeasurementBoardbyDeviceId,
    getMyMeasurementBoardbyDeviceId,
    getMyMeasurementBoardbyDeviceIdAndDate
};