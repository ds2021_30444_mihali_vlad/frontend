import axios from "axios";
import jwt_decode from "jwt-decode";

const API_URL = 'https://spring-demo-ds2020-vladmihali.herokuapp.com/';

const register = (username, password, name, address, birthDate) => {
  return axios.post(API_URL + "signup", {
    username,
    password,
    name,
    address,
    birthDate
  });
};

const login = (username, password) => {
  return axios
    .post(API_URL + "token/generate-token", {
      username,
      password,
    })
    .then((response) => {
      if (response.data.token) {
        localStorage.setItem("user", JSON.stringify(response.data));
      }

      return response.data;
    });
};

const logout = () => {
  localStorage.removeItem("user");
};

const getCurrentUser = () => {
    if(localStorage.getItem("user") == null){
        return null;
    }
  var decoded = jwt_decode(localStorage.getItem("user"));
  console.log(decoded);
  return jwt_decode(localStorage.getItem("user"));
};

export default {
  register,
  login,
  logout,
  getCurrentUser,
};