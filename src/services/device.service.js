import axios from "axios";
import authHeader from "./auth-header";

const API_URL = 'https://spring-demo-ds2020-vladmihali.herokuapp.com/'+"device";

const getDeviceBoard = () => {
  return axios.get(API_URL + "/", { headers: authHeader() });
};

const getMyDeviceBoard = () => {
    return axios.get(API_URL + "/my", { headers: authHeader() });
  };

const getDeviceBoardbyUserId = (userId) => {
    return axios.get(API_URL + "/user/"+userId, { headers: authHeader() });
};

const updateDevice = (newData) => {
    return axios.put(API_URL + "/"+newData.id, newData, { headers: authHeader() });
};

const addDevice = (newData) => {
    return axios.post(API_URL + "/", newData, { headers: authHeader() });
};

const deleteDevice = (id) => {
    return axios.delete(API_URL + "/"+id, { headers: authHeader() });
};
export default {
    getDeviceBoard,
    getMyDeviceBoard,
    updateDevice,
    addDevice,
    deleteDevice,
    getDeviceBoardbyUserId
};