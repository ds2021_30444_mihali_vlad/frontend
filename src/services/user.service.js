import axios from "axios";
import authHeader from "./auth-header";

const API_URL = 'https://spring-demo-ds2020-vladmihali.herokuapp.com/';

const getPublicContent = () => {
  return axios.get(API_URL, { headers: authHeader() });
};

const getUserBoard = () => {
  return axios.get(API_URL, { headers: authHeader() });
};

const getAdminBoard = () => {
  return axios.get(API_URL + "user/", { headers: authHeader() });
};

const updateUser = (newUser) => {
  return axios.put(API_URL + "user/"+newUser.id, newUser, { headers: authHeader() });
};

const deleteUser = (id) => {
  return axios.delete(API_URL + "user/"+id, { headers: authHeader() });
};

export default {
  getPublicContent,
  getUserBoard,
  getAdminBoard,
  updateUser,
  deleteUser
};