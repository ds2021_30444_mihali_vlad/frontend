import React, { Component, useEffect, useState } from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const data = [
    {
      name: 'Page A',
      uv: 4000,
      pv: 2400,
      amt: 2400,
    },
    {
      name: 'Page B',
      uv: 3000,
      pv: 1398,
      amt: 2210,
    },
    {
      name: 'Page C',
      uv: 2000,
      pv: 9800,
      amt: 2290,
    },
    {
      name: 'Page D',
      uv: 2780,
      pv: 3908,
      amt: 2000,
    },
    {
      name: 'Page E',
      uv: 1890,
      pv: 4800,
      amt: 2181,
    },
    {
      name: 'Page F',
      uv: 2390,
      pv: 3800,
      amt: 2500,
    },
    {
      name: 'Page G',
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
  ];

function MyChart() {
    const [dataPers, setDataPers] = useState(data);
    const [nr, setNr] = useState(0);
    function inc(){
        setNr((nr) => (nr + 1));
    }
    useEffect(()=>{
        console.log('m-am incarcat');
    }, []);
    useEffect(()=>{
        console.log(nr);
    }, [nr]);
    function handler() {
        setDataPers((dataPers) => {
            let copyData = JSON.parse(JSON.stringify(dataPers));
            copyData[0].uv = 12000;
            copyData[1].uv = 7000;
            copyData[2].uv = 25000;

            return copyData;
        });
        console.log(1);
    }
    return (
    <div style = {styles.container}>
      <ResponsiveContainer width="100%" height="100%">
        <LineChart
          width={500}
          height={300}
          data={dataPers}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="pv" stroke="#8884d8" activeDot={{ r: 8 }} />
          <Line type="monotone" dataKey="uv" stroke="#82ca9d" />
        </LineChart>
      </ResponsiveContainer>
      <button onClick={() => {handler();}}> Click me </button>
      <h2>nr: {nr}</h2>
      <button onClick={() => {inc();}}> Increment me </button>
    </div>
    );
}

const styles = {
    container : {
      background: "#FFFFFF",
      
      width: 500,
      height: 500
    }
  }

export default MyChart;