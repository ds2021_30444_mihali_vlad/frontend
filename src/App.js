import React, { useState, useEffect } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import AuthService from "./services/auth.service";

import Login from "./components/Login";
import Register from "./components/Register";
import Home from "./components/Home";
import Profile from "./components/Profile";
import BoardUser from "./components/BoardUser";
import BoardAdmin from "./components/BoardAdmin";
import BoardDevice from "./components/BoardDevice";
import BoardMeasurement from "./components/BoardMeasurement";
import DeviceByUserTable from "./components/DeviceByUserTable";
import MeasurementByDeviceTable from "./components/MeasurementByDeviceTable";
import MyMeasurementBoard from "./components/MyMeasurementBoard";
import MyCharts from "./components/MyCharts";
import MyDeviceBoard from "./components/MyDeviceBoard";
import * as SockJS from "sockjs-client";
import { Stomp } from "@stomp/stompjs";
import authHeader from "./services/auth-header";
import { Alert } from 'react-alert'
import { JSONRPCClient } from "json-rpc-2.0";




const App = () => {
  const [showAdminBoard, setShowAdminBoard] = useState(false);
  const [currentUser, setCurrentUser] = useState(undefined);
  const [stomp, setStomp] = useState(undefined);

  useEffect(() => {
    const user = AuthService.getCurrentUser();

    if (user) {
      setCurrentUser(user);
      setShowAdminBoard(user.vlad1234.includes("ROLE_ADMIN"));
    }


    // JSONRPCClient needs to know how to send a JSON-RPC request.
// Tell it by passing a function to its constructor. The function must take a JSON-RPC request and send it.
const client = new JSONRPCClient((jsonRPCRequest) =>
fetch('https://spring-demo-ds2020-vladmihali.herokuapp.com/'+"jsonrpc4j/api", {
  method: "POST",
  headers: {
    "content-type": "application/json",
  },
  body: JSON.stringify(jsonRPCRequest),
}).then((response) => {
  if (response.status === 200) {
    // Use client.receive when you received a JSON-RPC response.
    return response
      .json()
      .then((jsonRPCResponse) => client.receive(jsonRPCResponse));
  } else if (jsonRPCRequest.id !== undefined) {
    return Promise.reject(new Error(response.statusText));
  }
})
);

// Use client.request to make a JSON-RPC request call.
// The function returns a promise of the result.
client
.request("echo", { str: "Hello, World!" })
.then((result) => console.log(result));



    var socket = new SockJS('https://spring-demo-ds2020-vladmihali.herokuapp.com/'+'websocket');
    const stompClient = Stomp.over(socket);
    stompClient.connect({ headers: authHeader() }, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe("/topic/"+user?.sub+"/errenergy", function (event) {
          const json = JSON.parse(event.body);
            alert(json.message);
        });
    });
    setStomp(stompClient);
  }, []);

  const logOut = () => {
    AuthService.logout();
    stomp.disconnect();
  };

//give an initial state so that the data won't be undefined at start
// const ws = new WebSocket("wss://localhost:8080/topic/"+currentUser?.sub+"/errenergy");

// ws.onmessage = function (event) {
//   const json = JSON.parse(event.data);
//   try {
//     if ((json.event = "data")) {
//       console.log(event);
//       alert(json.data.message);
//     }
//   } catch (err) {
//     console.log(err);
//   }
// };



  return (
    <div>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <Link to={"/"} className="navbar-brand">
          Energy App
        </Link>
        <div className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to={"/home"} className="nav-link">
              Home
            </Link>
          </li>

          {showAdminBoard && (
            <li className="nav-item">
              <Link to={"/admin"} className="nav-link">
                Users
              </Link>
            </li>
          )}

          {showAdminBoard && (
            <li className="nav-item">
              <Link to={"/device"} className="nav-link">
                Devices
              </Link>
            </li>
          )}

          {showAdminBoard && (
            <li className="nav-item">
              <Link to={"/measurement"} className="nav-link">
                Measurements
              </Link>
            </li>
          )}

          {/* {currentUser && (
            <li className="nav-item">
              <Link to={"/user"} className="nav-link">
                User
              </Link>
            </li>
          )} */}
           {currentUser && (
            <li className="nav-item">
              <Link to={"/mydevices"} className="nav-link">
                My Devices
              </Link>
            </li>
          )}
        </div>  
        {currentUser ? (
          <div className="navbar-nav ml-auto">
            <li className="nav-item">
              <Link to={"/profile"} className="nav-link">
                {currentUser.username}
              </Link>
            </li>
            <li className="nav-item">
              <a href="/login" className="nav-link" onClick={logOut}>
                LogOut
              </a>
            </li>
          </div>
        ) : (
          <div className="navbar-nav ml-auto">
            <li className="nav-item">
              <Link to={"/login"} className="nav-link">
                Login
              </Link>
            </li>

            <li className="nav-item">
              <Link to={"/register"} className="nav-link">
                Sign Up
              </Link>
            </li>
          </div>
        )}
      </nav>

      <div className="container mt-3">
        <Switch>
          <Route exact path={["/", "/home"]} component={Home} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/profile" component={Profile} />
          <Route path="/user" component={BoardUser} />
          <Route path="/admin" component={BoardAdmin} />
          <Route path="/device" component={BoardDevice} />
          <Route path="/measurement" component={BoardMeasurement} />
          <Route path="/devices/user/:userId" component={DeviceByUserTable} />
          <Route path="/measurements/device/:deviceId" component={MeasurementByDeviceTable} />
          <Route path="/mymeasurements/device/:deviceId" component={MyMeasurementBoard} />
          <Route path="/mymeasurements/chart/device/:deviceId" component={MyCharts} />
          <Route path="/mydevices" component={MyDeviceBoard} />
        </Switch>
      </div>
    </div>
  );
};

export default App;
// import React, { Component } from 'react';
// import './App.css';

// import Person from './Person';
// import MyChart from './MyChart';


// class App extends Component {
//   render() {
//     return (
//       <div style = {styles.container}>
        
//         <Person 
//           style = {styles.person}
//           name = {"blasio99"}
//           age = {22}
//         />

//         <MyChart 

//         />

//       </div>
//     );
//   }
// }

// const styles = {
//   container : {
//     background: "#4B4A4A",
    
//     width: 1000,
//     height: 1000
//   },
//   person: {
//     background: "#464646",
//   }
// }

// export default App;
