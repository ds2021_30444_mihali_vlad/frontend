import React, { useState, useEffect } from "react";
import {useParams} from "react-router-dom";
import "react-datepicker/dist/react-datepicker.css";
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, Label } from 'recharts';
import { JSONRPCClient } from "json-rpc-2.0";

const client = new JSONRPCClient((jsonRPCRequest) =>
fetch('https://spring-demo-ds2020-vladmihali.herokuapp.com/'+"jsonrpc4j/api", {
  method: "POST",
  headers: {
    "Authorization": "Bearer " + JSON.parse(localStorage.getItem('user')).token,
    "content-type": "application/json",
  },
  body: JSON.stringify(jsonRPCRequest),
}).then((response) => {
  if (response.status === 200) {
    return response
      .json()
      .then((jsonRPCResponse) => client.receive(jsonRPCResponse));
  } else if (jsonRPCRequest.id !== undefined) {
    return Promise.reject(new Error(response.statusText));
  }
})
);

const MyCharts = () => {
  const { deviceId } = useParams();
  const [historyDays, setHistoryDays] = useState(7);
  const [programDuration, setProgramDuration] = useState(3);
  const [startProgram, setStartProgram] = useState(0);
  const [historyData, setHistoryData] = useState([]);
  const [baseline, setBaseline] = useState([]);
  const [newBaseline, setNewBaseline] = useState([]);

// Use client.request to make a JSON-RPC request call.
// The function returns a promise of the result.


  useEffect(() => {
    client
      .request("historicalEnergyConsumption", { days:7, device:deviceId })
      .then((result) => {
        console.log(result)
        setHistoryData(convertDataToChartForm(result, true))
      });

    client
      .request("averagedEnergyConsumption", { device:deviceId })
      .then((result) => {
        console.log(result)
        setBaseline(convertDataToChartForm(result, false))
      });

      client
      .request("startProgram", { duration:programDuration, device:deviceId })
      .then((result) => {
        console.log("start program "+result)
        setStartProgram(result)
      });

      client
      .request("newBaseline", { duration:programDuration, device:deviceId })
      .then((result) => {
        console.log("new baseline "+result)
        setNewBaseline(convertDataToChartForm(result, false))
      });
  }, []);

  function convertIndexToDateTime(h){
    return new Date(new Date() - (h+1)*60 * 60 * 1000)
  }

  function convertDataToChartForm(data, formatExtended){
    const result = [];
    for (let i = 0; i < data.length; i++) {
      let d
      if(formatExtended === true){
        d = {
          key: convertIndexToDateTime(i).toLocaleString('it-IT',{year: 'numeric', month: 'numeric', day: 'numeric',hour: '2-digit'}),
          value: data[i]
      };
      }else{
        d = {
          key: convertIndexToDateTime(i).toLocaleTimeString('it-IT',{hour: '2-digit'}),
          value: data[i]
        };
      }
      result.push(d);
    }
    return result
  }

  return (
    <div className="container" style = {styles.container}>
      <label for="days">Days: </label>
      <input id='days' style={{width: '150px'}} type="number" value={historyDays} onChange={(event) => {
        if(event.target.value>0){
        setHistoryDays(event.target.value)
        client
          .request("historicalEnergyConsumption", { days:event.target.value, device:deviceId })
          .then((result) => setHistoryData(convertDataToChartForm(result, true)));
        }
        console.log(event.target.value)}}/>
    <h3 style = {styles.title}>Historical Chart for {historyDays} days</h3>
      <ResponsiveContainer width="100%" height="100%" className="responsiveContainer">
        <LineChart
          width={500}
          height={500}
          data={historyData}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="key" interval={'preserveStartEnd'}/>
          <YAxis/>
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="value" stroke="#82ca9d" activeDot={{ r: 8 }} />
        </LineChart>
      </ResponsiveContainer>
      <br/><br/>
      <h3 style = {styles.title}>Baseline</h3>
      <ResponsiveContainer width="100%" height="100%">
        <LineChart
          width={500}
          height={500}
          data={baseline}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="key" interval={'preserveStartEnd'}/>
          <YAxis/>
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="value" stroke="#82ca9d" activeDot={{ r: 8 }} />
        </LineChart>
      </ResponsiveContainer>
      <br/><br/>
      <label for="programDuration">Program Duration: </label>
      <input id="programDuration" style={{width: '150px'}} type="number" value={programDuration} onChange={(event) => {
        if(event.target.value>0){
        setProgramDuration(event.target.value)
        client
          .request("startProgram", { duration:event.target.value, device:deviceId })
          .then((result) => setStartProgram(result));
        client
          .request("newBaseline", { duration:event.target.value, device:deviceId })
          .then((result) => setNewBaseline(convertDataToChartForm(result, false)));
        }
        console.log(event.target.value)}}/>
        <p>program starts at {convertIndexToDateTime(Number(startProgram)+Number(programDuration)).toLocaleTimeString('it-IT',{hour: '2-digit'})} and ends at {convertIndexToDateTime(startProgram).toLocaleTimeString('it-IT',{hour: '2-digit'})}</p>
        <h3 style = {styles.title}>new Baseline</h3>
      <ResponsiveContainer width="100%" height="100%">
        <LineChart
          width={500}
          height={500}
          data={newBaseline}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="key" interval={'preserveStartEnd'}/>
          <YAxis/>
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="value" stroke="#82ca9d" activeDot={{ r: 8 }} />
        </LineChart>
      </ResponsiveContainer>
    </div>
  );
};

const styles = {
    container : {
      background: "#FFFFFF",
      
      width: 1000,
      height: 500
    },
    title : {
      marginLeft: 'auto',
      marginRight: 'auto',
      textAlign: 'center'
    },
    responsiveContainer : {
        padding: '50000px'
       }
  }

export default MyCharts;