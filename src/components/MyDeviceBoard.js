import React, { useState, useEffect } from "react";
import { forwardRef } from 'react';
import DeviceService from "../services/device.service";
// import EventBus from "../common/EventBus";
import Avatar from 'react-avatar';
import Grid from '@material-ui/core/Grid';
import MaterialTable, { MTableBodyRow } from "material-table";
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import Alert from '@material-ui/lab/Alert';
import { useHistory } from "react-router-dom";
import ShowChartIcon from '@mui/icons-material/ShowChart';


const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

var columns = [
  {title: "id", field: "id", hidden: true},
  {title: "description", field: "description"},
  {title: "location", field: "location"},
  {title: "max", field: "max", type:'numeric'},
  {title: "average", field: "average", type:'numeric'},
  {title: "sensorDescription", field: "sensorDescription"},
  {title: "sensorMax", field: "sensorMax", type:'numeric'}
]



const MyDeviceBoard = () => { 
  const history = useHistory();
  const [data, setData] = useState([]);
  const [iserror, setIserror] = useState(false)
  const [errorMessages, setErrorMessages] = useState([])

  useEffect(() => {
    DeviceService.getMyDeviceBoard().then(
      (response) => {
        setData(response.data);
      },
      (error) => {
        const _data =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setData(_data);

        // if (error.response && error.response.status === 401) {
        //   EventBus.dispatch("logout");
        // }
      }
    );
  }, []);

  const handleRowUpdate = (newData, oldData, resolve) => {
    //validation
    let errorList = []
    if(newData.description === ""){
      errorList.push("Please enter description")
    }
    if(newData.location === ""){
      errorList.push("Please enter location")
    }
    if(newData.sensorDescription === ""){
      errorList.push("Please enter sensorDescription")
    }

    if(errorList.length < 1){
        DeviceService.updateDevice(newData)
      .then(res => {
        const dataUpdate = [...data];
        const index = oldData.tableData.id;
        dataUpdate[index] = newData;
        setData([...dataUpdate]);
        resolve()
        setIserror(false)
        setErrorMessages([])
      })
      .catch(error => {
        setErrorMessages(["Update failed! Server error"])
        setIserror(true)
        resolve()
        
      })
    }else{
      setErrorMessages(errorList)
      setIserror(true)
      resolve()

    }
    
  }

  const handleRowAdd = (newData, resolve) => {
    //validation
    let errorList = []
    if(newData.description === undefined){
        errorList.push("Please enter description")
      }
      if(newData.location === undefined){
        errorList.push("Please enter location")
      }
      if(newData.sensorDescription === undefined){
        errorList.push("Please enter sensorDescription")
      }
      if(newData.max === undefined){
        errorList.push("Please enter max")
      }
      if(newData.average === undefined){
        errorList.push("Please enter average")
      }
      if(newData.sensorMax === undefined){
        errorList.push("Please enter sensorMax")
      }

    if(errorList.length < 1){ //no error
        DeviceService.addDevice(newData)
      .then(res => {
        let dataToAdd = [...data];
        dataToAdd.push(res.data);
        setData(dataToAdd);
        resolve()
        setErrorMessages([])
        setIserror(false)
      })
      .catch(error => {
        setErrorMessages(["Cannot add data. Server error!"])
        setIserror(true)
        resolve()
      })
    }else{
      setErrorMessages(errorList)
      setIserror(true)
      resolve()
    }

    
  }

  const handleRowDelete = (oldData, resolve) => {
    
    DeviceService.deleteDevice(oldData.id)
      .then(res => {
        const dataDelete = [...data];
        const index = oldData.tableData.id;
        dataDelete.splice(index, 1);
        setData([...dataDelete]);
        resolve()
      })
      .catch(error => {
        setErrorMessages(["Delete failed! Server error"])
        setIserror(true)
        resolve()
      })
  }

  return (
    <div className="container">
      <Grid container spacing={100} item xs={600} item ys={600}>
          <div>
            {iserror && 
              <Alert severity="error">
                  {errorMessages.map((msg, i) => {
                      return <div key={i}>{msg}</div>
                  })}
              </Alert>
            }
          </div>
            <MaterialTable
              title="Devices"
              columns={columns}
              data={data}
              icons={tableIcons}
              actions={[
                {
                  icon: ()=> <ShowChartIcon/>,
                  tooltip: 'Save User',
                  onClick: (event, rowData) => {
                    // Do save operation
                    console.log("dadad "+rowData.id)
                    let path = `/mymeasurements/chart/device/${rowData.id}`; 
                      history.push(path);
                  }
                }
              ]}
              components={{
                Row: props => (
                  <MTableBodyRow
                    {...props}
                    onDoubleClick={(e) => {
                      console.log(props.data.id);
                      let path = `/mymeasurements/device/${props.data.id}`; 
                      history.push(path);
                    }}
                  />
                )
              }}
              editable={{
                // onRowUpdate: (newData, oldData) =>
                //   new Promise((resolve) => {
                //       handleRowUpdate(newData, oldData, resolve);
                      
                //   }),
                // // onRowAdd: (newData) =>
                // //   new Promise((resolve) => {
                // //     handleRowAdd(newData, resolve)
                // //   }),
                // onRowDelete: (oldData) =>
                //   new Promise((resolve) => {
                //     handleRowDelete(oldData, resolve)
                //   }),
              }}
            />
          </Grid>
    </div>
  );
};

export default MyDeviceBoard;