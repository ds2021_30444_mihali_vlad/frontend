import React from "react";
import AuthService from "../services/auth.service";

const Profile = () => {
  const currentUser = AuthService.getCurrentUser();

  return (
    <div className="container">
      <header className="jumbotron">
        <h3>
          <strong>Welcome to Energy APP!</strong>
        </h3>
      </header>
      <p>
        <strong>Username:</strong> {currentUser.sub}
      </p>
    </div>
  );
};

export default Profile;