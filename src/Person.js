import React, { Component } from 'react';

function Person(props) {

    return (
        <div style = {{...styles.container, ...props.style}}>
            
            <h2> Name: {props.name} </h2>

            <h2> Age: {props.age} </h2>
        </div>
    );
}

const styles = {
    container : {
      background: "#3B3A3A",
      
      width: 200,
      height: 200
    }
  }

export default Person;